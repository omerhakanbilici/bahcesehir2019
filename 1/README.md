INTRO
=====

Hello everyone, my name is Emin Mastizada, I'm substitute teacher for Guray Yildirim, I will substitute him for next 3 weeks/lectures.

I'm a Software Engineer in Greezli France, working as a Backend Developer, using mostly Django Web Framework,
but from time-to-time get involved in devops, project management and security improvements of the system.

Feel free to contact me at emin [at] linux.com

This week we are going to learn about how Web works :D
A simple information about how it works, what is the magic behind and what is needed for own website for World Wide Web (WWW).

It is mostly theoretical part.

Next week we will learn basics of python language, enough to write own website using python web frameworks.

And third week it will be about flask web framework, how it works, which functionalities it has and etc.
At the end of the lecture each student will try to build their own website.
You will use that site later with lecturer Guray on subjects like how to improve security of such web applications.

Lecture notes will be available in Gitlab repository, I will try to publish notes after each lecture.

url: gitlab.com/mastizada/bahcesehir2019/


# TCP/IP

TCP stands for Transmission Control Protocol and IP for Internet Protocol. These protocols define how data should travel between web nodes (computers) in internet.
How they should be packetized, addressed, transmitted, routed, and received.
If internet is a collection of streets, these protocols are transportation tools (bike, car, bus and etc.).

You might need to know that there are 2 main Transport layers we use in web applications, TCP and UDP.
TCP always verifies that data is delivered correctly and repeats if it isn't.
UDP however just sends the data without any verification from client's side. It might or might not arrive.

# DNS

Domain Name Servers. All resources in the internet are using ip addresses.
Domain Name Servers store ip addresses for domains and provide them to users. So instead of typing an ip address, you just memorize the domain name (ex: google.com).

Possible Security Threat: Due to caches in dns servers, if one of the nodes will be injected with incorrect ip address, then all sub nodes will be infected.
This how some of government blocks works and why using custom dns bypasses such censorships.

# HTTP

Hypertext Transfer Protocol is an application protocol that defines how servers and clients should communicate.

Basic schema example: protocol://user:password@server:port/path/?query=value#fragment

http://google.com/search?q=42#2

# Website Components

Clients (client-side): Accepts only files, it can be HTML, CSS, JavaScript (files recognized by browsers), web assets (image, video, music) or anything else (downloadable files).

# Steps

1. User enters a website name in the browser
2. Browser asks a dns server where this site is
3. DNS server finds IP address, caches it for future requests and replies with an IP address
4. Browser establishes a TCP connection with that IP address, asks for the provided path (/, /blog/, /item/ and etc.)
5. Server finds that path, if everything is ok, returns its HTML file with a response code 200
6. If there are additional media in the HTML file (CSS, JavaScript, Image, Video and etc) browser asks server for each of them one-by-one (improved in HTTP/2 and HTTP/3)
7. Browser closes connection with server.

# HTTPS

Https stands for "HTTP Secure".
By default the HTTP protocol is naked. An intruder might intercept in a communication between you and the server and dump (read) all messages (requests and responses).
To make the communication secure, HTTPS uses SSL (Secure Sockets Layer) or TLS (Transport Layer Security) layers.
Using these layers, client verifies the identity of the server it is connected to, and the server also verified identity of messages coming from the client.
Also these layers help to encrypt the communication data between client and server.

# HTTP Methods
HTTP has few methods to communicate with a server: GET, POST (main methods), PUT, PATCH, DELETE
GET method is used to request a path from the server and POST method is used to send data to the server.

# Backend and Frontend

So, as you now know, clients (browsers) only understand simple files (HTML, CSS, JavaScript). This part is usually called "Front-end".
To process the data, manage permissions, users and etc. you will need a backend part that can execute a code (C/C++, Python, Ruby, Java, PHP and etc.).

# Common Gateway Interface (CGI)

Backend codes are executed in a CGI and then replies to the client with according data.

It includes doing calculations in backend, generating response codes (200, 202, 400, 403, 404, 500 and etc.), managing header flags and generating the response file (HTML, JSON, XML and etc.)
that will be returned to the client.

# Web Frameworks

Developing a CGI application is extremely hard. It requires implementing HTTP Protocol details for making packages (methods, headers, response codes, data) and
is not used in modern web applications.

To handle such bases, Web Frameworks can be used.
A Web Framework includes HTTP method handling and some of them have much more features like Database handling, template engines and etc.

After learning how to build a web application using a web framework, you will focus on making it much secure.
Modern web frameworks handle some of the securty needes, but problems like SQL-injections but you still need to learn how to handle such problems in your website.

To learn all these, we will use Flask framework which is writren in Python language, have small set of features to give you better control over your application.

# Lecture slides

Lecture slides are also available in the same repository, it is mostly useful for describing notes.

